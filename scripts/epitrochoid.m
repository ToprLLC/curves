FixedR=0.4
RollingR=1.0-FixedR
PointOffset=RollingR * 1.1

t=0.0:0.1:100.0
x=(FixedR+RollingR)*cos(t) - PointOffset*cos(t*(FixedR+RollingR)/RollingR)
y=(FixedR+RollingR)*sin(t) - PointOffset*sin(t*(FixedR+RollingR)/RollingR)
plot(x,y)