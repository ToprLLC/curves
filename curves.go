package main

import (
	"flag"
	"fmt"
	"strings"

	_ "bitbucket.org/curves/fynevisual"
	"bitbucket.org/curves/visual"
)

func init() {
	//fynevisual.Hello()
}
func main() {
	flag.Parse()

	if flag.NArg() < 1 {
		fmt.Println("usage: curves [fyne|gtk]")
		return
	}

	guitype := "fyne"
	if flag.NArg() >= 1 {
		if strings.Compare(flag.Arg(0), "gtk") == 0 {
			guitype = "gtk"
		}
	}

	vis := visual.Lookup(guitype)
	vis.Play()
}
