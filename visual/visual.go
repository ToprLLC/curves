package visual

import (
	"fmt"
)

const (
	TOOLBARHEIGHT = 50
)

type Visualizer interface {
	Play()
}

var visualizers map[string](Visualizer)

func init() {
	fmt.Println("Visual")
	if visualizers == nil {
		visualizers = make(map[string]Visualizer)
	}
}
func Register(name string, v Visualizer) {
	visualizers[name] = v
	fmt.Printf("Registering %s\n", name)
}

func Lookup(name string) Visualizer {
	fmt.Printf("Looking up %s\n", name)
	return visualizers[name]
}
