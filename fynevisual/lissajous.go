package fynevisual

import (
	"image"
	"image/draw"
	"math"

	"bitbucket.org/curves/curve"
	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
)

type lissFields struct {
	A         *widget.Entry
	B         *widget.Entry
	abRatio   *widget.Slider
	phaseDiff *widget.Slider

	image image.Image

	plot *canvas.Raster

	curve *curve.Lissajous
}

func (c *lissFields) Next() {
	c.curve.AddPoints()
	c.plot.Refresh()
}

func lissParamChanged(val float64) {
	var arg curve.LissArgument
	arg.NumPoints = 200

	abRatio := curves.lissFields.abRatio.Value
	arg.Alpha = 1.0
	arg.Beta = abRatio * arg.Alpha

	arg.Phase = curves.lissFields.phaseDiff.Value

	arg.A = 1.0
	arg.B = 2.0

	deltat := math.Pi / 64

	arg.Tnow = -2 * math.Pi
	arg.Tdelta = deltat

	curves.lissFields.curve = curve.NewLissajous(arg)
	curves.lissFields.plot.Refresh()
	curve.ServerChannel <- &curves.lissFields

}

func lissPainter(w, h int) image.Image {
	if curves.lissFields.curve == nil {
		lissParamChanged(0.0)
	}
	curves.lissFields.image = curves.lissFields.curve.UpdatePlot(3*h/4, 3*h/4)
	upLeft := image.Point{0, 0}
	lowRight := image.Point{w, h}

	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})
	tgt := image.Rectangle{Min: image.Point{X: w/2 - h/2, Y: 0}, Max: image.Point{X: (w + h) / 2, Y: h}}
	draw.Draw(img, tgt, curves.lissFields.image, image.Point{0, 0}, draw.Src)
	return img

}

func createLissajousTab() *widget.TabItem {

	lblrat := widget.NewLabel("ab Ratio")
	curves.lissFields.abRatio = widget.NewSlider(-1.0, 1.0)
	curves.lissFields.abRatio.Step = 0.1
	curves.lissFields.abRatio.Value = 0.5
	curves.lissFields.abRatio.OnChanged = lissParamChanged

	lblblank1 := widget.NewLabel("           ")

	lblphase := widget.NewLabel("Phase Difference")
	curves.lissFields.phaseDiff = widget.NewSlider(0.0, 2*math.Pi)
	curves.lissFields.phaseDiff.Step = 0.1
	curves.lissFields.phaseDiff.Value = 0.5
	curves.lissFields.phaseDiff.OnChanged = lissParamChanged
	lblblank2 := widget.NewLabel("            ")

	lines := fyne.NewContainerWithLayout(layout.NewGridLayout(3), lblrat, curves.lissFields.abRatio, lblblank1,
		lblphase, curves.lissFields.phaseDiff, lblblank2)

	frames := fyne.NewContainerWithLayout(layout.NewVBoxLayout(),
		lines)

	curves.lissFields.plot = canvas.NewRaster(lissPainter)
	curves.lissFields.plot.SetMinSize(fyne.Size{Width: 100, Height: 100})
	fulltab := fyne.NewContainerWithLayout(layout.NewBorderLayout(frames, nil, nil, nil), frames, curves.lissFields.plot)

	tabitem1 := widget.NewTabItem("Lissajous", fulltab)

	return tabitem1
}
