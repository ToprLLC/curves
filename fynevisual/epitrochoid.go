package fynevisual

import (
	"image"
	"image/draw"

	"bitbucket.org/curves/curve"
	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
)

type epitrochoidFields struct {
	radiiSlider  *widget.Slider
	offsetSlider *widget.Slider

	fromt   *widget.Entry
	tot     *widget.Entry
	image   image.Image
	plotBtn *widget.Button
	plot    *canvas.Raster

	curve *curve.Epitrochoid
}

var arg curve.EpitrochoidArgument

func (c *epitrochoidFields) Next() {
	//fmt.Println("Adding points")
	c.curve.AddPoints()
	c.plot.Refresh()
}

func epitrochoidParamChanged(val float64) {
	//fmt.Println("ParamChanged")
	arg.NumPoints = 200

	radiiValue := curves.epitrochoidFields.radiiSlider.Value
	arg.FixedR = radiiValue
	arg.RollingR = 1.0 - radiiValue
	arg.PointOffset = curves.epitrochoidFields.offsetSlider.Value

	//arg.Fromt, _ = strconv.ParseFloat(curves.epitrochoidFields.fromt.Text, 64)
	//arg.Tot, _ = strconv.ParseFloat(curves.epitrochoidFields.tot.Text, 64)
	arg.Fromt = 0.0
	arg.Tot = 1.0
	//fmt.Printf("Two diameters are %f, %f\n", arg.FixedR, arg.RollingR)
	curves.epitrochoidFields.curve = curve.NewEpiTrochoid(arg)
	curves.epitrochoidFields.plot.Refresh()
	curve.ServerChannel <- &curves.epitrochoidFields
	//go update()
}

func epitrochoidPainter(w, h int) image.Image {
	if curves.epitrochoidFields.curve == nil {
		epitrochoidParamChanged(0.0)
	}
	curves.epitrochoidFields.image = curves.epitrochoidFields.curve.UpdatePlot(3*h/4, 3*h/4)
	upLeft := image.Point{0, 0}
	lowRight := image.Point{w, h}

	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})
	tgt := image.Rectangle{Min: image.Point{X: w/2 - h/2, Y: 0}, Max: image.Point{X: (w + h) / 2, Y: h}}
	draw.Draw(img, tgt, curves.epitrochoidFields.image, image.Point{0, 0}, draw.Src)
	return img

}

func createEpitrochoidTab() *widget.TabItem {

	lblrad := widget.NewLabel("Radii")
	curves.epitrochoidFields.radiiSlider = widget.NewSlider(0.0, 1.0)
	curves.epitrochoidFields.radiiSlider.Step = 0.1
	curves.epitrochoidFields.radiiSlider.Value = 0.5
	curves.epitrochoidFields.radiiSlider.OnChanged = epitrochoidParamChanged

	lblblank1 := widget.NewLabel("           ")

	lbloff := widget.NewLabel("Offset")
	curves.epitrochoidFields.offsetSlider = widget.NewSlider(0.0, 1.0)
	curves.epitrochoidFields.offsetSlider.Step = 0.1
	curves.epitrochoidFields.offsetSlider.Value = 0.5
	curves.epitrochoidFields.offsetSlider.OnChanged = epitrochoidParamChanged
	lblblank2 := widget.NewLabel("            ")

	lines := fyne.NewContainerWithLayout(layout.NewGridLayout(3), lblrad, curves.epitrochoidFields.radiiSlider, lblblank1,
		lbloff, curves.epitrochoidFields.offsetSlider, lblblank2)

	frames := fyne.NewContainerWithLayout(layout.NewVBoxLayout(),
		lines)

	curves.epitrochoidFields.plot = canvas.NewRaster(epitrochoidPainter)
	curves.epitrochoidFields.plot.SetMinSize(fyne.Size{Width: 100, Height: 100})
	fulltab := fyne.NewContainerWithLayout(layout.NewBorderLayout(frames, nil, nil, nil), frames, curves.epitrochoidFields.plot)

	tabitem1 := widget.NewTabItem("Epitrochoid", fulltab)

	return tabitem1
}
