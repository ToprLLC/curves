package fynevisual

import (
	"fmt"

	"bitbucket.org/curves/curve"
	"bitbucket.org/curves/visual"
	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/widget"
)

type Curves struct {
	MainWindow fyne.Window

	lissajousButton *widget.Button
	lissFields      lissFields

	harmonographButton *widget.Button

	epitrochoidButton *widget.Button
	epitrochoidForm   *widget.Form
	epitrochoidFields epitrochoidFields

	stripchartButton *widget.Button

	curveCanvas *canvas.Image
}

var curves Curves

func init() {
	fmt.Printf("Registering fyne\n")
	visual.Register("fyne", &curves)
}

func curveSelected(item *widget.TabItem) {
	fmt.Printf("Tab %s selected\n", item.Text)
	curve.ServerChannel <- nil
}

func (c *Curves) Play() {

	myapp := app.New()
	curves.MainWindow = myapp.NewWindow("Curves")

	tabitem := createLissajousTab()
	tabitem1 := createEpitrochoidTab()

	label2 := widget.NewLabel("Harmonograph")
	tabitem2 := widget.NewTabItem("Harmonograph", label2)

	tab := widget.NewTabContainer(tabitem, tabitem1, tabitem2)
	tab.OnChanged = curveSelected

	curves.MainWindow.SetContent(tab)
	curves.MainWindow.ShowAndRun()

}

func Hello() {

}
