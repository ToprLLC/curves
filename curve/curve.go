package curve

import (
	"fmt"
	"os"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
)

var Verbosity int

type Point struct {
	X float64
	Y float64
}

type Argument struct {
	NumPoints int
}

type SignalArgument struct {
	Argument
	Frequency    float64
	Phase        float64
	SamplingFreq float64
}

type Curve struct {
	Name   string
	Points []Point
	XMin   float64
	XMax   float64
	YMin   float64
	YMax   float64
	Plot   *plot.Plot
}

type Signal struct {
	Name   string
	Points []Point
}

func (c *Curve) Show() {
	fmt.Printf("Name: %s\n", c.Name)
	if Verbosity > 2 {
		for idx, pt := range c.Points {
			fmt.Printf("%3d : %f %f\n", idx, pt.X, pt.Y)
		}
	} else {
		fmt.Printf("Number of Points: %d\n", len(c.Points))
	}
}

func (c *Curve) Scilab() {
	csvfile, _ := os.Create(c.Name + ".csv")
	defer csvfile.Close()

	for _, pt := range c.Points {
		fmt.Fprintf(csvfile, "%f %f\n", pt.X, pt.Y)
	}
}

func (c *Curve) Init() {
	p, err := plot.New()
	if err != nil {
		panic(err)
	}
	p.Add(plotter.NewGrid())
	c.Plot = p
}
