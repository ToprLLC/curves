package curve

import (
	"math"
)

// References:
//         https://en.wikipedia.org/wiki/Lissajous_curve
//         https://mathworld.wolfram.com/LissajousCurve.html

type LissArgument struct {
	Argument
	A      float64
	Alpha  float64
	B      float64
	Beta   float64
	Phase  float64
	Tnow   float64
	Tdelta float64
}

type Lissajous struct {
	Curve
	Arg LissArgument
}

func NewLissajous(arg LissArgument) *Lissajous {
	newcurve := new(Lissajous)
	newcurve.Name = "lissajous"
	newcurve.Arg = arg

	newcurve.XMin = -1.5
	newcurve.XMax = 1.5
	newcurve.YMin = -1.5
	newcurve.YMax = 1.5

	newcurve.Points = make([]Point, arg.NumPoints)

	newcurve.AddPoints()
	return newcurve
}

func (e *Lissajous) Value(t float64) (x, y float64) {
	arg := e.Arg
	x = arg.A * math.Cos(t*arg.Alpha+arg.Phase)
	y = arg.B * math.Cos(t*arg.Beta)
	return x, y
}

func (e *Lissajous) AddPoints() {
	t := e.Arg.Tnow
	deltat := e.Arg.Tdelta
	for i := 0; i < e.Arg.NumPoints-1; i++ {
		x, y := e.Value(t)
		e.Points[i] = Point{X: x, Y: y}
		t = t + deltat
	}
	t = t + deltat
	x, y := e.Value(t)

	e.Points[e.Arg.NumPoints-1] = Point{X: x, Y: y}
	e.Arg.Tnow = t + deltat

}
