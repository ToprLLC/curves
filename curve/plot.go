package curve

import (
	"image"

	"golang.org/x/image/colornames"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgimg"
)

func convert(points []Point) plotter.XYs {
	pts := make(plotter.XYs, len(points))
	var minX, maxX float64
	for i, pt := range points {
		pts[i].X = pt.X
		pts[i].Y = pt.Y
		if pt.X < minX {
			minX = pt.X
		}
		if pt.X > maxX {
			maxX = pt.X
		}
	}

	return pts
}

func (c *Curve) PlotToFile(title string, annotation string) {

	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = c.Name
	pts := convert(c.Points)

	err = plotutil.AddLinePoints(p, pts)
	if err != nil {
		panic(err)
	}

	// Save the plot to a PNG file.
	outfname := c.Name + ".png"
	if err := p.Save(6*vg.Inch, 6*vg.Inch, outfname); err != nil {
		panic(err)
	}

}

func (c *Curve) PlotToImage(w, h int) image.Image {

	p, err := plot.New()
	if err != nil {
		panic(err)
	}
	p.Add(plotter.NewGrid())

	p.Title.Text = c.Name
	pts := convert(c.Points)

	err = plotutil.AddLinePoints(p, pts)
	if err != nil {
		panic(err)
	}

	cnv := vgimg.New(vg.Length(w), vg.Length(h))
	p.Draw(draw.New(cnv))

	return cnv.Image()
}

func (c *Curve) UpdatePlot(w, h int) image.Image {
	var err error
	if c.Plot == nil {
		c.Plot, err = plot.New()
		c.Plot.X.Min = c.XMin
		c.Plot.X.Max = c.XMax
		c.Plot.Y.Min = c.YMin
		c.Plot.Y.Max = c.YMax
	}
	pts := convert(c.Points)
	line, _ := plotter.NewLine(pts)
	line.LineStyle.Color = colornames.Green
	line.LineStyle.Width = 3

	//err = plotutil.AddLinePoints(c.Plot, line)
	c.Plot.Add(line)
	if err != nil {
		panic(err)
	}

	cnv := vgimg.New(vg.Length(w), vg.Length(h))
	newcnv := draw.New(cnv)
	c.Plot.HideAxes()
	c.Plot.Draw(newcnv)

	return cnv.Image()

}
