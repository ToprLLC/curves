package curve

import (
	"fmt"
	"testing"

	"gonum.org/v1/plot/vg"
)

func TestEpitrochoid(t *testing.T) {
	Verbosity = 15
	var arg EpitrochoidArgument
	arg.FixedR = 10.0
	arg.RollingR = 1.0
	arg.PointOffset = 10.0
	arg.NumPoints = 2000
	arg.Fromt = -50.0
	arg.Tot = 50.0
	et := NewEpiTrochoid(arg)
	et.Scilab()
	et.PlotToFile("epitrochoid", "annotate")
}

func TestEpitrochoidUpdate(t *testing.T) {
	Verbosity = 15
	var arg EpitrochoidArgument
	arg.FixedR = 10.0
	arg.RollingR = 1.0
	arg.PointOffset = 10.0
	arg.NumPoints = 200
	arg.Fromt = -5.1
	arg.Tot = -5.0
	et := NewEpiTrochoid(arg)
	for i := 0; i < 10; i++ {
		et.AddPoints()
		et.UpdatePlot(500, 500)
		outfname := fmt.Sprintf("et%d.png", i)
		et.Plot.Save(6*vg.Inch, 6*vg.Inch, outfname)
	}
}

func BenchmarkEpitrochoid(b *testing.B) {
	var arg EpitrochoidArgument
	for bn := 0; bn < b.N; bn++ {
		arg.FixedR = 10.0
		arg.RollingR = 1.0
		arg.PointOffset = 10.0
		arg.NumPoints = 200
		arg.Fromt = -5.1
		arg.Tot = -5.0
		et := NewEpiTrochoid(arg)
		for i := 0; i < 10; i++ {
			et.AddPoints()
			et.UpdatePlot(500, 500)
		}
	}
}
