package curve

import "math"

// References:
//         https://en.wikipedia.org/wiki/Harmonograph
//         https://mathworld.wolfram.com/Harmonograph.html

type Component struct {
	A         float64
	Frequency float64
	Phase     float64
	D         float64
}
type HarmonographArgument struct {
	Argument
	Components [4]Component
	Fromt      float64
	Tot        float64
}

type Harmonograph struct {
	Curve
	Arg HarmonographArgument
}

func NewHarmonograph(arg HarmonographArgument) *Harmonograph {
	newcurve := new(Harmonograph)
	newcurve.Name = "harmonograph"
	newcurve.Arg = arg
	newcurve.Points = make([]Point, arg.NumPoints)

	t := arg.Fromt
	deltat := (arg.Tot - arg.Fromt) / float64(arg.NumPoints)

	for i := 0; i < arg.NumPoints-1; i++ {
		part1 := arg.Components[0].A * math.Sin(t*arg.Components[0].Frequency+arg.Components[0].Phase) * math.Exp(-arg.Components[0].D*t)
		part2 := arg.Components[1].A * math.Sin(t*arg.Components[1].Frequency+arg.Components[1].Phase) * math.Exp(-arg.Components[1].D*t)
		newcurve.Points[i].X = part1 + part2

		part1 = arg.Components[2].A * math.Sin(t*arg.Components[2].Frequency+arg.Components[2].Phase) * math.Exp(-arg.Components[2].D*t)
		part2 = arg.Components[3].A * math.Sin(t*arg.Components[3].Frequency+arg.Components[3].Phase) * math.Exp(-arg.Components[3].D*t)
		newcurve.Points[i].Y = part1 + part2
		t = t + deltat
	}
	return newcurve
}
