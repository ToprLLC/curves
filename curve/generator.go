package curve

import (
	"fmt"
	"time"
)

type Generator interface {
	Next()
}

var ServerChannel chan Generator

func init() {
	ServerChannel = make(chan Generator)
	Server()
}
func Server() {
	go func() {
		var gen Generator
		for {
			select {
			case gen = <-ServerChannel:
				fmt.Println("Curve changed")
			case <-time.After(200 * time.Millisecond):

				if gen != nil {
					//fmt.Println("Timer")
					gen.Next()
				}
			}
		}
	}()
}
