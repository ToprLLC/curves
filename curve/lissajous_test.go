package curve

import (
	"math"
	"testing"

	"gonum.org/v1/plot/vg"
)

func TestLissajous(t *testing.T) {
	Verbosity = 15
	var arg LissArgument
	arg.A = 2.0
	arg.Alpha = 5.0
	arg.B = 1.0
	arg.Beta = 3.0
	arg.Phase = math.Pi / 4.0
	arg.NumPoints = 128

	deltat := math.Pi / 64

	arg.Tnow = -2 * math.Pi
	arg.Tdelta = deltat

	et := NewLissajous(arg)
	for i := 0; i < 150; i++ {
		et.UpdatePlot(64, 64)
	}
	et.Plot.Save(6*vg.Inch, 6*vg.Inch, "lissajous.png")
}
