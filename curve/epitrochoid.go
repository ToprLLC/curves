package curve

import (
	"math"
)

// References:
//         https://en.wikipedia.org/wiki/Epitrochoid
//         https://mathworld.wolfram.com/Epitrochoid.html

type EpitrochoidArgument struct {
	Argument
	FixedR      float64
	RollingR    float64
	PointOffset float64
	Fromt       float64
	Tot         float64
}

type Epitrochoid struct {
	Curve
	Arg    EpitrochoidArgument
	Tnow   float64
	Tdelta float64
}

func NewEpiTrochoid(arg EpitrochoidArgument) *Epitrochoid {
	newcurve := new(Epitrochoid)
	newcurve.Name = "epitrochoid"
	newcurve.Arg = arg

	newcurve.XMin = -1.5
	newcurve.XMax = 1.5
	newcurve.YMin = -1.5
	newcurve.YMax = 1.5
	newcurve.Points = make([]Point, arg.NumPoints)
	deltat := (arg.Tot - arg.Fromt) / float64((arg.NumPoints - 1))

	t := arg.Fromt
	newcurve.Tnow = t + deltat
	newcurve.Tdelta = deltat
	newcurve.AddPoints()
	return newcurve
}

func (e *Epitrochoid) Value(t float64) (x, y float64) {
	x = (e.Arg.FixedR+e.Arg.RollingR)*math.Cos(t) - e.Arg.PointOffset*math.Cos(t*((e.Arg.FixedR+e.Arg.RollingR)/e.Arg.RollingR))
	y = (e.Arg.FixedR+e.Arg.RollingR)*math.Sin(t) - e.Arg.PointOffset*math.Sin(t*((e.Arg.FixedR+e.Arg.RollingR)/e.Arg.RollingR))
	return x, y
}

func (e *Epitrochoid) AddPoints() {
	t := e.Tnow
	deltat := e.Tdelta
	for i := 0; i < e.Arg.NumPoints-1; i++ {
		x, y := e.Value(t)
		e.Points[i] = Point{X: x, Y: y}
		t = t + deltat
	}
	t = t + deltat
	x, y := e.Value(t)

	e.Points[e.Arg.NumPoints-1] = Point{X: x, Y: y}
	e.Tnow = t + deltat

}
