package curve

import (
	"math"
	"testing"
)

func TestHarmonograph(t *testing.T) {
	Verbosity = 15
	var arg HarmonographArgument

	arg.NumPoints = 10000
	arg.Fromt = -5.0
	arg.Tot = 5.0

	//var A1 = 100, f1 = 2, p1 = 1/16, d1 = 0.02;
	var comp Component
	comp.A = 100.0
	comp.D = 0.02
	comp.Frequency = 10.0
	comp.Phase = math.Pi / 6.0

	arg.Components[0] = comp

	comp.Frequency = comp.Frequency + 1.0
	comp.D = comp.D + 0.01
	arg.Components[1] = comp
	comp.Frequency = comp.Frequency + 1.0
	comp.D = comp.D + 0.01
	arg.Components[2] = comp
	comp.Frequency = comp.Frequency + 1.0
	comp.D = comp.D + 0.01
	arg.Components[3] = comp

	h := NewHarmonograph(arg)
	h.Scilab()
	h.PlotToFile("title", "annotation")
}
func BenchmarkHarmonograph(b *testing.B) {
	Verbosity = 15
	var arg HarmonographArgument
	for bn := 0; bn < b.N; bn++ {
		arg.NumPoints = 10000
		arg.Fromt = -5.0
		arg.Tot = 5.0

		//var A1 = 100, f1 = 2, p1 = 1/16, d1 = 0.02;
		var comp Component
		comp.A = 100.0
		comp.D = 0.02
		comp.Frequency = 10.0
		comp.Phase = math.Pi / 6.0

		arg.Components[0] = comp

		comp.Frequency = comp.Frequency + 1.0
		comp.D = comp.D + 0.01
		arg.Components[1] = comp
		comp.Frequency = comp.Frequency + 1.0
		comp.D = comp.D + 0.01
		arg.Components[2] = comp
		comp.Frequency = comp.Frequency + 1.0
		comp.D = comp.D + 0.01
		arg.Components[3] = comp

		h := NewHarmonograph(arg)
		h.Scilab()
		h.PlotToFile("title", "annotation")
	}
}
