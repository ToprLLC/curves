module bitbucket.org/curves

go 1.14

require (
	fyne.io/fyne v1.3.2
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8
	gonum.org/v1/plot v0.7.0
)
